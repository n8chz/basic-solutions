require 'minitest/autorun'
require_relative 'lp'

class LPTest < Minitest::Test
  def test_4_6_2
    m = 1000000
    tableau = [
      [1,-4,-2,-3,-5,m,m,0],
      [0,2,3,4,2,1,0,300],
      [0,8,1,1,5,0,1,300]
    ]
    x = LP.from_tableau(tableau).simplex(slim=false)
    puts "x* = #{x}"
    assert true
  end

  def test_4_6_1
    skip
    m = 1000000
    tableau = [
      [1,-2,-3,0,m,0],
      [0,1,2,1,0,4],
      [0,1,1,0,1,3]
    ]
    x = LP.from_tableau(tableau).simplex(slim=false)
    puts "x* = #{x}"
    assert true
  end

  def test_4_1_7
    skip
    m = 1000000
    tableau = [
      [-1,7-5*m,8-7*m,0,0,0,2*m,2*m,2*m,-120*m],
      [ 0,    2,    3,1,0,0, -1,  0,  0,    42],
      [ 0,    3,    4,0,1,0,  0, -1,  0,    60],
      [ 0,    1,    1,0,0,1,  0,  0, -1,    18]
    ]
    x = LP.from_tableau(tableau).simplex(slim=false)
    puts "x* = #{x}"
    assert true
  end

  def test_ic3
    skip
    m = 1000000
    tableau = [
      [-1, 6000, 7000, 2000, 0,0,0,0,0,-m,-m,-m,-m,0],
      [0,6,2,0,1,0,0,0,0,-1,0,0,0,24],
      [0,2,2,0,0,1,0,0,0,0,-1,0,0,28],
      [0,4,10,0,0,0,1,0,0,0,0,-1,0,25],
      [0,1,-1,1,0,0,0,1,0,0,0,0,-1,0],
      [0,1,-1,-1,0,0,0,0,1,0,0,0,0,0]
    ]
    assert LP.from_tableau(tableau).simplex(slim=false)
  end

  def test_problem_5
    skip
    m = 1000000
    tableau = [
      [-1, 3, 2, 4, -m, 0, m, 0],
      [0, 2, 1, 3, 1, 0, 0, 60],
      [0, 3, 3, 5, 0, -1, 1, 120]
    ]
    assert LP.from_tableau(tableau).simplex(slim=true)
  end

  def test_problem_4
    skip
    a = [
      [1, -2, 4, 3],
      [-4, 6, 5, -3],
      [2, -3, 3, 8]
    ]
    b = [20, 40, 50]
    c = [5, 1, 3, 4]
    assert LP.new(a, b, c).simplex(slim=true)
  end
  
  # a homework problem
  def test_problem_1
    skip
    a = [
      [0, 1],
      [2, 5],
      [1, 1],
      [3, 1],
    ]
    b = [10, 60, 18, 44]
    c = [2, 1]
    actual = LP.new(a, b, c).simplex(slim=true)
    expected = [13, 5]
    assert_equal expected, actual
  end

  def test_algebraic_interpretation
    skip
    a = [
      [0, 1],
      [2, 5],
      [1, 1],
      [3, 1],
    ]
    b = [10, 60, 18, 44]
    c = [2, 1]
    assert LP.new(a, b, c).algebraic_interpretation
  end

  # see Hillier, Lieberman, "Operations Research", 10th ed., p. 28
  def test_wyndor_glass
    skip
    a = [[1, 0], [0, 2], [3, 2]]
    b = [4, 12, 18]
    c = [3, 5]
    actual = LP.new(a, b, c).simplex
    expected = [2, 6]
    assert_equal expected, actual
  end

  # Ibid, p. 47
  def test_radiation_therapy
    skip # it fails
    a = [
      [Rational(3, 10), Rational(1, 10)],
      [Rational(1, 2), Rational(1, 2)],
      [-Rational(1, 2), -Rational(1, 2)],
      [-Rational(3, 5), -Rational(2, 5)]
    ]
    b = [Rational(27, 10), 6, -6, -6]
    c = [-Rational(2, 5), -Rational(1, 2)]
    # TODO actual, expected, test
    expected = [Rational(15, 2), Rational(9, 2)]
    actual = LP.new(a, b, c).simplex
    assert_equal expected, actual
  end

  # Ibid, p. 49
  def test_kibbutzim
    skip # endless loop
    f3 = Rational(1, 300)
    f4 = Rational(1, 400)
    f6 = Rational(1, 600)
    a = [
      [1, 0, 0, 1, 0, 0, 1, 0, 0],
      [0, 1, 0, 0, 1, 0, 0, 1, 0],
      [0, 0, 1, 0, 0, 1, 0, 0, 1],
      [3, 0, 0, 2, 0, 0, 1, 0, 0],
      [0, 3, 0, 0, 2, 0, 0, 1, 0],
      [0, 0, 3, 0, 0, 2, 0, 0, 1],
      [1, 1, 1, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 1, 1, 1, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 1, 1, 1],
      [f4, -f6, 0, f4, -f6, 0, f4, -f6, 0],
      [0, f6, -f3, 0, f6, -f3, 0, f6, -f3],
      [-f4, 0, f3, -f4, 0, f3, -f4, 0, f3]
    ]
    b = [400, 600, 300, 600, 800, 375, 600, 500, 325, 0, 0, 0]
    c = [1000, 1000, 1000, 750, 750, 750, 250, 250, 250]
    expected = [Rational(400, 3), 100, 25, 100, 250, 150, 0, 0, 0]
    actual = LP.new(a, b, c).simplex
    assert_equal expected, actual
  end
  
  # Ibid, p. 53
  def test_air_pollution
    skip
    puts "air_pollution_test checking in"
    a = [
      [-12, -9, -25, -20, -17, -13],
      [-35, -42, -18, -31, -56, -49],
      [-37, -53, -28, -24, -29, -20],
      [1, 0, 0, 0, 0, 0],
      [0, 1, 0, 0, 0, 0],
      [0, 0, 1, 0, 0, 0],
      [0, 0, 0, 1, 0, 0],
      [0, 0, 0, 0, 1, 0],
      [0, 0, 0, 0, 0, 1]
    ]
    b = [-60, -150, -125, 1, 1, 1, 1, 1, 1]
    c = [-8, -10, -7, -6, -11, -9]
    expected = [1, 0.623, 0.343, 1, 0.048, 1]
    actual = LP.new(a, b, c).simplex
    assert_equal expected, actual
  end

=begin
  # Ibid, p. 57
  def solid_wastes_test
    skip
  end
=end
end
