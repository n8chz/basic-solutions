A program to list feasible basic solutions for a
linear programming tableau, given a matrix of coefficients
***A*** and a column vector ***b***.

`a.basic(b)` returns an array of the vectors that are basic solutions.

'a.feasible_basic(b)` returns that subset of the basic solution set
that are feasible, that is, have no negative entries.
