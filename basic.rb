#!/usr/bin/ruby

require "matrix"

class Matrix
  def basic(b)
    column_vectors.zip(0..).combination(rank).map do |entry|
      vecs, indices = entry.transpose
      [self.class.columns(vecs), indices]
    end.reject do |m, i|
      m.singular?
    end.map do |m, i|
      intersperse(m.inv*b, i)
    end
  end

  def feasible_basic(b)
    basic(b).select {|v| v.none?(&:negative?)}
  end

  private def intersperse(x, indices)
    value = Vector.zero(column_count)
    x.zip(indices).each do |x_i, i|
      value[i] = x_i
    end
    value
  end

end
